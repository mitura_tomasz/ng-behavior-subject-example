import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/behaviorSubject";
import { Subject } from "rxjs/Subject";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WorkService {

  $workTime: Subject<number> = new BehaviorSubject<number>(100);

  constructor() { }

  getWorkTime(): Observable<number> {
    return this.$workTime.asObservable();
  }

  setWorkTime(hours: number): void {
    this.$workTime.next(hours);
  }

}
