import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyComponent } from './company.component';
import { BossComponent } from './components/boss/boss.component';
import { EmployeeComponent } from './components/employee/employee.component';

import { WorkService } from './../../services/work.service';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CompanyComponent, BossComponent, EmployeeComponent],
  exports: [CompanyComponent],
  providers: [WorkService]
})
export class CompanyModule { }
